package cn.edu.sjzc;

public class SuccessFail {
	// 判断胜负的方法
	public boolean success(String[][] e, int i, int j) {

		int x = 1, count = 1;
		while (i + x < e.length && e[i][j] == e[i + x][j]) {
			x++;
			count++;
		}

		int x1 = 1;
		while (i - x1 > 0 && e[i][j] == e[i - x1][j]) {
			x1++;
			count++;
		}

		if (count >= 5) { // 如果胜出，返回false，
			return false;
		}

		int x2 = 1, count2 = 1;
		while (j + x2 < e[0].length && e[i][j] == e[i][j + x2]) {
			x2++;
			count2++;
		}

		int x22 = 1;
		while (j - x22 > 0 && e[i][j] == e[i][j - x22]) {
			x22++;
			count2++;
		}

		 if (count2 >= 5) {
			return false;
		}

		int x3 = 1, count3 = 1;
		while (i + x3 < e.length && j + x3 < e[0].length&& e[i][j] == e[i + x3][j + x3]) {
			x3++;
			count3++;
		}

		int x33 = 1;
		while (i - x33 > 0 && j - x33 > 0 && e[i][j] == e[i - x33][j - x33]) {
			x33++;
			count3++;
		}

		 if (count3 >= 5) {
			return false;
		}

		int x4 = 1, count4 = 1;
		while (i + x4 < e.length && j - x4 > 0
				&& e[i][j] == e[i + x4][j - x4]) {
			x4++;
			count4++;
		}
		int x44 = 1;
		while (i - x44 > 0 && j + x44 < e[0].length
				&& e[i][j] == e[i - x44][j + x44]) {
			x44++;
			count4++;
		}
		 if (count4 >= 5) {
			return false;
		} else {
			return true;
		}

	}
}
