package cn.edu.sjzc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeMap;

public class ChessMan {
	
	int count = 0; // 记录下棋的次数，若奇数次，轮到甲方下棋，否则乙方
	int z1 = 1,z2 = 1;
	TreeMap<Integer,String> tm1 = new TreeMap<Integer,String>();
	TreeMap<Integer,String> tm2 = new TreeMap<Integer,String>();
	TreeMap<Integer,String> tm3 = new TreeMap<Integer,String>();
	TreeMap<Integer,String> tm4 = new TreeMap<Integer,String>();

	public String[][] chessman(int i,int j,String[][] c) throws IOException {
		ChessBoard qi = new ChessBoard(i,j);
		SuccessFail s = new SuccessFail(); 
		Iput q3 = new Iput();
		JContinue jc = new JContinue();
		boolean p = true;
		
		while (count < (i-1) * (j-1) && p) { // 棋盘没满并且没人胜出
			int m, n;
			if (count == 0) {
				System.out.println("游戏说明:");
				System.out.println("甲方：◆      乙方：◎");
				System.out.println("请输入：");
				System.out.println("行坐标为1到" + (i-1) + "的数字");
				System.out.println("纵坐标为1到" + (j-1) + "的数字");
				System.out.println("");
			}

			// 定义键盘接收数据并且count(count初始为0)为偶数，甲方执棋，提示甲方输入，否则，乙方执棋
			if (count % 2 == 0) {
				System.out.println("甲方执棋");
				m = q3.io();
				n = q3.io();
			} else {
				System.out.println("乙方执棋");
				m = q3.io();
				n = q3.io();
			}

			// 输入完m和n后判断m和n是否越界
			if (m > 0 && n > 0 && m < i && n < j) { // 若没越界，判断是否已有棋子
				if (c[m][n] == "◆" || c[m][n] == "◎") {
					System.out.println("此处已有棋子，请重新输入。。。");
					System.out.println("");
				} else { 
				
					    count++; // count=1
					if (count % 2 != 0) { // 甲方 count奇数 棋子◆
						c[m][n] = "◆";
						Date d = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat("下棋子时间："+"yyyy-MM-dd HH:mm:ss");
						tm1.put(z1,m+","+n);
						tm3.put(z1,sdf.format(d));
						z1++;
					} else {
						c[m][n] = "◎";
						Date d = new Date();
						SimpleDateFormat sdf = new SimpleDateFormat("下棋子时间："+"yyyy-MM-dd HH:mm:ss");	
						tm2.put(z2,m+","+n);
						tm4.put(z2,sdf.format(d));
						z2++;
					}
					qi.print(c);
					p = s.success(c, m, n);
				}

	
			} else { // 如果输入的值为负数，提示越界
				System.out.println("越界，请重新输入。。。");
				System.out.println("注意：");
				System.out.println("行坐标为1到" + (i-1) + "的数字");
				System.out.println("纵坐标为1到" + (j-1) + "的数字");
				System.out.println("");
				System.out.println("");
				qi.print(c);
			}
		} // while循环结束
		
		

		if (count == (i-1) * (j-1)) {                 
			System.out.println("棋盘已满");
			count = 0;  //棋盘满后，count初始化为0，以使重新开始游戏时，count为0
		}

		// 返回true时程序执行这里，说明有人胜出，接着判断是甲方还是乙方胜出
		int jf = 0;
		if (count>=9&&count % 2 != 0) {
			System.out.println("甲方胜出！");
			count = 0;
			System.out.println("");
		    jf = 1;
		}
		if (count>=9&& count % 2 == 0) {
			System.out.println("乙方胜出！");
			count = 0;                        //乙方胜出后，count初始化为0，以使重新开始游戏时，count为0
			System.out.println("");
		    jf = 0;
		}
		
		Iterator it1 = tm1.keySet().iterator();
		Iterator it3 = tm3.keySet().iterator();
		System.out.println("甲方");
		int h1 = 1;
		while(it1.hasNext()){		
				System.out.println("第"+h1+"步："+tm1.get(it1.next())+tm3.get(it3.next()));
				try{
					Date d = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");	
					String filename = "d:/text/"+sdf.format(d)+".txt";
				String s1 = "";
				String s2 = "";
				s1 = tm1.get(h1);
				s2 = tm3.get(h1);
				FileOutputStream fos = new FileOutputStream(filename,true);
				String[] strArray = s1.split(",");
   	   		    byte[] buff = strArray[0].getBytes();
   	   		    byte[] buff1 = strArray[1].getBytes();
   	   		    String str1 = "甲方第"+h1+"步：";
   	   		    byte[] buff2 = str1.getBytes();
   	   		    String str3 = ",";
   	   		    byte[] buff3 = str3.getBytes();
   	   		    byte[] buff4 = s2.getBytes();
   	   		    fos.write(buff2);
   	   		    fos.write(buff);
   	   		    fos.write(buff3);
   	            fos.write(buff1);
   	            fos.write(buff4);
   	            String str = "\r\n";
			    byte[] buf = str.getBytes();
			    fos.write(buf);
   	            fos.close();
				}catch(FileNotFoundException e1){
					System.out.print(e1);
				}catch(IOException e2){
					System.out.print(e2);
				}
			h1++;
		}
		
		Iterator it2 = tm2.keySet().iterator();
		Iterator it4 = tm4.keySet().iterator();
		System.out.println("乙方");
		int h2 = 1;
		while(it2.hasNext()){
				System.out.println("第"+h2+"步："+tm2.get(it2.next())+tm4.get(it4.next()));
				try{
					Date d = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");	
					String filename = "d:/text/"+sdf.format(d)+".txt";
				String s1 = "";
				String s2 = "";
				s1 = tm2.get(h2);
				s2 = tm4.get(h2);
				String[] strArray = s1.split(",");
				FileOutputStream fos = new FileOutputStream(filename,true);
   	   		    byte[] buff = strArray[0].getBytes();
   	   		    byte[] buff1 = strArray[1].getBytes();
   	   		    String str1 = "乙方第"+h2+"步：";
   	   		    byte[] buff2 = str1.getBytes();
   	   		    String str3 = ",";
   	        	byte[] buff3 = str3.getBytes();
   	        	byte[] buff4 = s2.getBytes();
   	   		    fos.write(buff2);
   	   		    fos.write(buff);
   	   		    fos.write(buff3);
   	            fos.write(buff1);
   	             fos.write(buff4);
   	            String str = "\r\n";
				byte[] buf = str.getBytes();
				fos.write(buf);
   	            fos.close();
				}catch(FileNotFoundException e1){
					System.out.print(e1);
				}catch(IOException e2){
					System.out.print(e2);
				}
			h2++;
		}    
		
		if(jf ==1){
			System.out.print("乙方是否想悔棋？");
			System.out.println("是1  否0 ");
	   		System.out.println("请输入0或1");
	   		int m = q3.io();
	   		if(m == 1){
	   		    c= qi.initiate();
	   			System.out.print("悔棋到第几步？");
	   			int m1 = q3.io();
	   			for(int u=1;u<=m1;u ++){
	   				String s1 = "";
	   				s1=tm1.get(u);
	   	   			String[] strArray = s1.split(",");
	   	   		    int m2 = Integer.parseInt(strArray[0]);
	   	   		    int n2 = Integer.parseInt(strArray[1]);
	   	   			c[m2][n2] = "◆";
	   	   			z1 = m1+1;
	   			}
	   			
	   			for(int u=1;u<m1;u ++){
	   				String s2 = "";
	   				s2=tm2.get(u);
	   	   			String[] strArray = s2.split(",");
	   	   		    int m2 = Integer.parseInt(strArray[0]);
	   	   		    int n2 = Integer.parseInt(strArray[1]);
	   	   		 	c[m2][n2] = "◎";
	   	   		 	z2 = m1;
	   			}
	   			qi.print(c);
	   			count =2*m1-1;
	   			chessman(c.length,c[0].length,c);
		}
	   		if(m == 0){
	   			jc.con();   
	   		}
   			
   		}
		
		
		if(jf ==0){
			System.out.print("甲方是否想悔棋？");
			System.out.println("是1  否0 ");
	   		System.out.println("请输入0或1");
	   		int m = q3.io();
	   		if(m == 1){
	   		    c= qi.initiate();
	   			System.out.print("悔棋到第几步？");
	   			int m1 = q3.io();
	   			for(int u=1;u<m1;u ++){
	   				String s1 = "";
	   				s1=tm1.get(u);
	   	   			String[] strArray = s1.split(",");   	            
	   	   		    int m2 = Integer.parseInt(strArray[0]);
	   	   		    int n2 = Integer.parseInt(strArray[1]);
	   	   			c[m2][n2] = "◆";
	   	   			z1 = m1;
	   			}
	   			
	   			for(int u=1;u<m1;u++){
	   				String s2 = "";
	   				s2=tm2.get(u);
	   	   			String[] strArray = s2.split(",");
	   	   		    int m2 = Integer.parseInt(strArray[0]);
	   	   		    int n2 = Integer.parseInt(strArray[1]);
	   	   		 	c[m2][n2] = "◎";
	   	   		 	z2 = m1;
	   			}
	   			qi.print(c);
	   			count = 2*(m1-1);
	   			chessman(c.length,c[0].length,c);
		}
	   		if(m == 0){
	   			jc.con();   
	   		}
   			
   		}

		return c;  

	}
}
