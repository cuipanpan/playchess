package cn.edu.sjzc;

public class ChessBoard {

	/**
	 * 用一个棋盘类把棋盘封装起来
	 * 
	 * @param args
	 * cuipanpan 2013-7-18
	 */

	// 棋盘的大小及get()方法
	private int length;
	private int width;
	private String[][] a;

	// 棋盘的构造方法
	public ChessBoard(int length, int width) {
		this.length = length;
		this.width = width;
	}

	
	// 初始化棋盘方法
	public String[][] initiate() {
		a = new String[length][width];
		for (int i = 1; i < length; i++) {
			for (int j = 1; j < width; j++) {
				a[i][j] = "╋";
			}
		}
		return a;
	}
	

	// 打印棋盘方法
	public void print(String[][] d) {
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < width; j++) {
				if (i == 0 && j == 0) {
					System.out.print("  ");
				} else if (i == 0 && j != 0) {
					if (j <= 9) {                       // 打印行号时，判断行号是否大于9
						System.out.print(j + " ");
					} else {                            // 行号大于9时，不输出空格
						System.out.print(j);
					}
				} else if (i != 0 && j == 0) {
					if (i <= 9) {                       // 打印列号时，判断列号是否大于9
						System.out.print(" " + i);        //列号小于9时，在前面输出空格
					} else {
						System.out.print(i);             //列号小于9时，在前面不输出空格
					}
				} else {
					System.out.print(d[i][j]);
				}

			}

			System.out.println("");
		}

	}
}
